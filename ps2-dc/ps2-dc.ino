#include <PS2X_lib.h>

#define ML_PWM_PIN 3
#define ML_DIR_PIN 2

#define MR_PWM_PIN 11
#define MR_DIR_PIN 12

PS2X ps2x;

int error;

int motorMin = 0;
int motorMax = 255;

int motorLeft;
int motorRight;

void setup() {
  Serial.begin(57600);

  pinMode(ML_PWM_PIN, OUTPUT);
  pinMode(ML_DIR_PIN, OUTPUT);

  pinMode(MR_PWM_PIN, OUTPUT);
  pinMode(MR_DIR_PIN, OUTPUT);

  error = ps2x.config_gamepad(4, 7, 6, 8, false, false); // pins: clock, command, attention, data, pressure, rumble

  if (error == 0) {
    Serial.println("Found controller, configuration successful");
    //TODO: print gamepad bindings
  } else if (error == 1) {
    Serial.println("No controller found");
  } else if (error == 2) {
    Serial.println("Controller found but not accepting commands");
  } else if (error == 3) {
    Serial.println("Controller does not support pressures mode");
  }
}

void loop() {

  if (error == 1) {
    return;
  }

  ps2x.read_gamepad(false, 0);

  if(ps2x.Analog(PSS_LY) > 128){
    motorLeft = map(ps2x.Analog(PSS_LY), 128, 255, motorMax, motorMin);
    analogWrite(ML_PWM_PIN, motorLeft);
    digitalWrite(ML_DIR_PIN, 0);
  } else if(ps2x.Analog(PSS_LY) < 128){
    motorLeft = map(ps2x.Analog(PSS_LY), 128, 255, motorMax, motorMin);
    analogWrite(ML_PWM_PIN, motorLeft);
    digitalWrite(ML_DIR_PIN, 1);
  }

  if(ps2x.Analog(PSS_RY) > 128){
    motorRight = map(ps2x.Analog(PSS_RY), 128, 255, motorMax, motorMin);
    analogWrite(MR_PWM_PIN, motorRight);
    digitalWrite(MR_PWM_PIN, 0);
    } else if(ps2x.Analog(PSS_RY) < 128){
      motorRight = map(ps2x.Analog(PSS_RY), 128, 255, motorMax, motorMin);
      analogWrite(MR_PWM_PIN, motorRight);
      digitalWrite(MR_DIR_PIN, 1);
    }

  Serial.println(ps2x.Analog(PSS_LY));
  Serial.println(ps2x.Analog(PSS_RY));
  Serial.println( );
  delay(50);
}
